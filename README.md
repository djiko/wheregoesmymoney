# Where goes my money?

## What is _Where goes my money?_?

Do you always know what you do with your money? I simply don't. _Where goes my money?_ is an attempt to provide a simple way to keep track of your expenses. Simple as in...simple: you don't need to know if you used your credit card or cash to know that your money is gone. It's simply gone.

_Where goes my money?_ is free software (as in free beer and free speech as well), published under a GNU GPL V3 licence.

## Qu'est ce que _Where goes my money?_?

Savez vous toujours ce que vous faîtes de votre argent ? Personnellement je n'y arrive pas. _Where goes my money?_ est un application qui vous permet de conserver une trace de vos dépenses. Aussi simple que...simple: vous n'avez pas besoin de savoir si vous avez utilisé votre carte de crédit ou du liquide. Votre argent n'est juste plus là.

_Where goes my money?_ est un logiciel libre et gratuit distribué sous les termes de la licence GNU GPL v3.
