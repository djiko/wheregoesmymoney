package com.net.djiko.where.db;

import android.provider.BaseColumns;

public class DbContract {

    private DbContract() {}

    public static class ExpenseEntry implements BaseColumns {
        public static final String TABLE_EXPENSES = "Expenses";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_PURPOSE = "purpose";

        public static String[] getColumns(){
            return new String[]{COLUMN_AMOUNT, COLUMN_PURPOSE, COLUMN_DATE};
        }
    }
}
