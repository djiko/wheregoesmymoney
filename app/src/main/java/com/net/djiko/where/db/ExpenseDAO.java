package com.net.djiko.where.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.net.djiko.where.models.Expense;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExpenseDAO {

    private final static String TAG = "EXPENSEDAO";

    private SQLiteDatabase mDatabase;
    private DatabaseHelper mDatabaseHelper;
    private Context mContext;

    private final String[] mColumns = DbContract.ExpenseEntry.getColumns();

    public ExpenseDAO(Context context){
        mDatabaseHelper = DatabaseHelper.getInstance(context);
        mContext = context;
    }

    private void openDatabase() throws SQLException {
        mDatabase = mDatabaseHelper.getWritableDatabase();
    }

    private void closeDatabase() {
        mDatabase.close();
    }

    public long save(Expense expense) {
        ContentValues values = new ContentValues();
        values.put(DbContract.ExpenseEntry.COLUMN_AMOUNT, expense.getAmount());
        values.put(DbContract.ExpenseEntry.COLUMN_PURPOSE, expense.getPurpose());
        values.put(DbContract.ExpenseEntry.COLUMN_DATE, expense.getDate().getTime());
        openDatabase();
        long expenseID = mDatabase.insert(DbContract.ExpenseEntry.TABLE_EXPENSES, null, values);
        closeDatabase();
        return expenseID;
    }

    public Expense get(long id){
        return getExpenseFromID(id);
    }

    public List<Expense> getExpensesBetween(Date startDate, Date endDate){
        List<Expense> expenses = new ArrayList<>();
        String query = "SELECT * FROM ";
        query += DbContract.ExpenseEntry.TABLE_EXPENSES;
        query += " WHERE ";
        query += DbContract.ExpenseEntry.COLUMN_DATE;
        query += " BETWEEN ";
        query += startDate.getTime();
        query += " AND ";
        query += endDate.getTime();
        query += ";";

        Log.i(TAG, "SQL query - expenses between: " + query);

        openDatabase();
        try (Cursor cursor = mDatabase.rawQuery(query, null)) {
            while (cursor.moveToNext()) {
                Expense expense = Expense.newInstance(cursor.getInt(1), new Date(cursor.getLong(2)), cursor.getString(3), mContext);
                expenses.add(expense);
            }
        }
        closeDatabase();
        return expenses;
    }

    private Expense getExpenseFromID(long id)
    {
        int amount;
        String purpose;
        Date date;
        Expense expense = null;
        openDatabase();
        Cursor cursor = mDatabase.query(DbContract.ExpenseEntry.TABLE_EXPENSES, null, "_id = " + id, null, null, null, null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            amount = cursor.getInt(1);
            date = new Date(cursor.getInt(2));
            purpose = cursor.getString(3);
            expense = Expense.newInstance(amount, date, purpose, mContext);
        }
        cursor.close();
        closeDatabase();
        return expense;
    }

    public Expense getLatestExpense() {
        Expense expense = Expense.newInstance(0, null, null, mContext);
        String query = "SELECT * FROM ";
        query += DbContract.ExpenseEntry.TABLE_EXPENSES;
        query += " WHERE ";
        query += DbContract.ExpenseEntry.COLUMN_DATE;
        query += " = (SELECT max(date) from ";
        query += DbContract.ExpenseEntry.TABLE_EXPENSES;
        query += ") ";
        query += " ORDER BY date desc ";
        query += " limit 1";
        query += ";";

        Log.i(TAG, "SQL - latest expense: " + query);

        openDatabase();
        try (Cursor cursor = mDatabase.rawQuery(query, null)) {
            while (cursor.moveToNext()) {
                expense.setAmount(cursor.getInt(1));
                expense.setDate(new Date(cursor.getLong(2)));
                expense.setPurpose(cursor.getString(3));
            }
        }
        closeDatabase();
        return expense;
    }
}
