package com.net.djiko.where;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.net.djiko.where.db.DatabaseHelper;
import com.net.djiko.where.db.FakeExpenses;
import com.net.djiko.where.models.Expense;

import java.util.Calendar;
import java.util.Date;

public class DashboardActivity extends AppCompatActivity {

    private Expense newExpense;
    private int added_amount = 0;
    private String added_category;

    private TextView mCurrentMonthAmountView;
    private TextView mPreviousMonthAmountView;
    private TextView mLatestExpenseView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DatabaseHelper mDatabaseHelper = DatabaseHelper.getInstance(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDialog dialog = getAddExpenseDialog();
                dialog.show();
            }
        });

        mCurrentMonthAmountView = (TextView) findViewById(R.id.monthly_amount);
        mPreviousMonthAmountView = (TextView) findViewById(R.id.previous_monthly_amount);
        mLatestExpenseView = (TextView) findViewById(R.id.latest_expenses_text);
        updateUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    private void updateUI() {
        if (getExpensesForCurrentMonth() > 0) {
            mCurrentMonthAmountView.setText(String.format(getString(R.string.monthly_amount_text), getExpensesForCurrentMonth() / 100));
        } else {
            mCurrentMonthAmountView.setText(getString(R.string.monthly_amount_text_empty));
        }

        if (getExpensesForPreviousMonth() > 0) {
            mPreviousMonthAmountView.setText(String.format(getString(R.string.previous_month_amount), getExpensesForPreviousMonth() / 100));
        } else {
            mPreviousMonthAmountView.setText(getString(R.string.previous_month_amount_empty));
        }

        if (Expense.getLatestExpense(this).getAmount() > 0) {
            Expense expense = Expense.getLatestExpense(this);
            mLatestExpenseView.setText(String.format(getString(R.string.latest_spending), (double) expense.getAmount() / 100, expense.getPurpose()));
        } else {
            mLatestExpenseView.setText(getString(R.string.latest_spending_empty));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private double getExpensesForCurrentMonth(){
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMinimum(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date firstDay = cal.getTime();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59);
        cal.set(Calendar.MILLISECOND, 999);
        Date lastDay = cal.getTime();
        return Expense.getExpensesAmountBetween(firstDay, lastDay, this);
    }

    private double getExpensesForPreviousMonth(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMinimum(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date firstDay = cal.getTime();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59);
        cal.set(Calendar.MILLISECOND, 999);
        Date lastDay = cal.getTime();
        return Expense.getExpensesAmountBetween(firstDay, lastDay, this);
    }

    private MaterialDialog getAddExpenseDialog(){

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.add_expense)
                .customView(R.layout.add_layout_dialog, false)
                .positiveText(R.string.add)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        EditText amountEditText = (EditText) dialog.findViewById(R.id.input_expense_amount);
                        added_amount = Integer.valueOf(amountEditText.getText().toString()) * 100;
                        EditText categoryEditText = (EditText) dialog.findViewById(R.id.input_expense_category);
                        added_category = categoryEditText.getText().toString().length() > 0 ? categoryEditText.getText().toString() : "";
                        newExpense = Expense.newInstance(added_amount, Calendar.getInstance().getTime(), added_category, DashboardActivity.this);
                        newExpense.save();
                        updateUI();
                    }
                })
                .build();

        final View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        positiveAction.setEnabled(false);
        EditText amountEditText = (EditText) dialog.findViewById(R.id.input_expense_amount);
        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                positiveAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return dialog;
    }

    public void displayDetailsForCurrentMonth(View view) {
        Toast.makeText(this, "Nothing to see here yet", Toast.LENGTH_SHORT).show();
    }

    private void loadFakeExpenses() {
        FakeExpenses expenses = new FakeExpenses(this);
    }
}
