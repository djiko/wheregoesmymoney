package com.net.djiko.where.db;

import android.content.Context;

import com.net.djiko.where.models.Expense;

import java.util.Calendar;
import java.util.Date;

public class FakeExpenses {

    public FakeExpenses(Context context) {
        int[] amounts = {
                1000, 2000, 2657, 9876, 5678,
                8643, 8764, 7657, 6734, 9876,
                1000, 2000, 2657, 9876, 5678,
                8643, 8764, 7657, 6734, 9876,
                1000, 2000, 2657, 9876, 5678,
                8643, 8764, 7657, 6734, 9876,
                1000, 2000, 2657, 9876, 5678,
                8643, 8764, 7657, 6734, 9876
        };

        String[] purposes = {
                "acheter une Tesla", "faire les courses", "payer le jardinier", "me droguer", "payer le coiffeur à prix d\'or",
                "acheter une Tesla", "faire les courses", "payer le jardinier", "me droguer", "payer le coiffeur à prix d\'or",
                "acheter une Tesla", "faire les courses", "payer le jardinier", "me droguer", "payer le coiffeur à prix d\'or",
                "acheter une Tesla", "faire les courses", "payer le jardinier", "me droguer", "payer le coiffeur à prix d\'or",
                "acheter une Tesla", "faire les courses", "payer le jardinier", "me droguer", "payer le coiffeur à prix d\'or",
                "acheter une Tesla", "faire les courses", "payer le jardinier", "me droguer", "payer le coiffeur à prix d\'or",
                "acheter une Tesla", "faire les courses", "payer le jardinier", "me droguer", "payer le coiffeur à prix d\'or",
                "acheter une Tesla", "faire les courses", "payer le jardinier", "me droguer", "payer le coiffeur à prix d\'or",
        };

        for (int i = 0; i < amounts.length; i++) {
            Expense expense = Expense.newInstance(amounts[i], new Date(), purposes[i], context);
            expense.save();
        }

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMinimum(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date date = cal.getTime();

        for (int i = 0; i < amounts.length - 5; i++) {
            Expense expense = Expense.newInstance(amounts[i], date, purposes[i], context);
            expense.save();
        }
    }
}
