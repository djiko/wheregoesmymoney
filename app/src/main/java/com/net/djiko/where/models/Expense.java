package com.net.djiko.where.models;

import android.content.Context;
import android.util.Log;

import com.net.djiko.where.db.ExpenseDAO;

import java.util.Date;
import java.util.List;

public class Expense {

    private int amount;
    private Date date;
    private String purpose;
    private Context context;

    private Expense(){}

    public static Expense newInstance(int amount, Date date, String purpose, Context context) {
        Expense expense = new Expense();
        expense.setAmount(amount);
        expense.setPurpose(purpose);
        expense.setDate(date);
        expense.setContext(context);
        return expense;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose= purpose;
    }

    public void save(){
        ExpenseDAO dao = new ExpenseDAO(context);
        long id = dao.save(this);
        Log.i("SAVED", String.valueOf(id));
    }

    public static List<Expense> getExpensesBetween(Date firstDay, Date lastDay, Context context) {
        List<Expense> expenses;
        ExpenseDAO dao = new ExpenseDAO(context);
        expenses = dao.getExpensesBetween(firstDay, lastDay);
        return expenses;
    }

    public static double getExpensesAmountBetween(Date firstDay, Date lastDay, Context context) {
        List<Expense> expenses = getExpensesBetween(firstDay, lastDay, context);
        int amount = 0;
        for (Expense expense : expenses)
            amount += expense.getAmount();
        return amount;
    }

    public static Expense getLatestExpense(Context context) {
        ExpenseDAO dao = new ExpenseDAO(context);
        return dao.getLatestExpense();
    }
}
