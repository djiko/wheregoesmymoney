package com.net.djiko.where.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import static com.net.djiko.where.db.DbContract.ExpenseEntry.COLUMN_PURPOSE;
import static com.net.djiko.where.db.DbContract.ExpenseEntry.TABLE_EXPENSES;
import static com.net.djiko.where.db.DbContract.ExpenseEntry.COLUMN_AMOUNT;
import static com.net.djiko.where.db.DbContract.ExpenseEntry.COLUMN_DATE;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper staticInstance;

    private static final String DB_NAME = "com.net.djiko.where.db";
    private static final int DB_VERSION = 1;

    private static final String CREATE_TABLE = "CREATE TABLE %s (";
    private static final String INTEGER = " INTEGER";
    private static final String TEXT = " TEXT";
    private static final String NOT_NULL = " NOT NULL";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS ";

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (staticInstance == null){
            staticInstance = new DatabaseHelper(context);
        }
        return staticInstance;
    }

    private DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        onUpgrade(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(DatabaseHelper.class.getName(), String.format("Upgrading from version %d to %d", oldVersion, newVersion));

        if (oldVersion > DB_VERSION) {
            Log.d(DatabaseHelper.class.getName(), String.format("Unsupported database version %d; recreating from scratch", oldVersion));
            dropAll(db);
            oldVersion = 0;
        }
        if (oldVersion < 1) createV1(db);
    }

    private void dropAll(SQLiteDatabase db) {
        db.execSQL(DROP_TABLE + TABLE_EXPENSES);
    }

    private void createV1(SQLiteDatabase db) {
        String sql = String.format(CREATE_TABLE, TABLE_EXPENSES)
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_AMOUNT + INTEGER + NOT_NULL + ","
                + COLUMN_DATE + INTEGER + ","
                + COLUMN_PURPOSE + TEXT + ")";
        Log.d("SQL-EXPENSE", sql);
        db.execSQL(sql);
    }
}
